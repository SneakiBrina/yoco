CREATE DATABASE yoco;

USE yoco;

CREATE TABLE `accounts` (
  `accountID` INT NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `permissionRank` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`accountID`)
);

CREATE TABLE `tickets` (
  `ticket` VARCHAR(40) NOT NULL,
  `clientHash` VARCHAR(40) NOT NULL,
  `lastActivity` INT NOT NULL,
  `accountID` INT NOT NULL,
  PRIMARY KEY (`ticket`),
  FOREIGN KEY FK_TICKETS_ACCOUNTID(accountID) REFERENCES accounts(accountID)
);

CREATE TABLE `chats` (
  `chatID` INT NOT NULL AUTO_INCREMENT,
  `chatName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`chatID`)
);

CREATE TABLE `chatLines` (
  `lineID` INT NOT NULL AUTO_INCREMENT,
  `chatID` INT NOT NULL,
  `accountID` INT NOT NULL,
  `time` DATETIME NOT NULL,
  `languageCode` VARCHAR(10) NOT NULL,
  `message` TEXT NOT NULL,
  PRIMARY KEY (`lineID`),
  FOREIGN KEY FK_CHATLINES_ACCOUNTID(accountID) REFERENCES accounts(accountID),
  FOREIGN KEY FK_CHATLINES_CHATID(chatID) REFERENCES chats(chatID)
);

CREATE TABLE `emails` (
  `emailID` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(254) NOT NULL,
  `from` varchar(254) NOT NULL,
  `subject` text NOT NULL,
  `headers` text NOT NULL,
  `content` text NOT NULL,
  `timeQueued` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timeSent` timestamp NULL DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`emailID`),
  KEY `timeQueuedIndex` (`timeQueued`)
);

CREATE TABLE `errors` (
  `errorID` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `message` text NOT NULL,
  `comment` text NOT NULL,
  `backtrace` text NOT NULL,
  `reported` int(1) NOT NULL,
  `times` int(11) NOT NULL,
  `seen` int(1) NOT NULL,
  PRIMARY KEY (`errorID`)
);

CREATE TABLE `settings` (
  `settingName` varchar(45) NOT NULL,
  `settingValue` text NOT NULL,
  PRIMARY KEY (`settingName`),
  UNIQUE KEY `settingName_UNIQUE` (`settingName`)
);

CREATE TABLE `loginAttempts` (
  `attemptID` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `user` varchar(45) NOT NULL,
  `result` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `useragent` text NOT NULL,
  PRIMARY KEY (`attemptID`)
);

INSERT INTO `accounts`
(`userName`, `password`, `permissionRank`)
VALUES ('Bigmack', 'notset', 10);

INSERT INTO `chats`
(`chatName`)
VALUES ('All');