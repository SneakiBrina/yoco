'use strict';
var GulpConfig = (function () {
    function GulpConfig() {
        this.root = './public_html';

        this.tsOutputPath = './public_html';
        this.allJavaScript = [this.root + '/**/*.js'];
        this.allTypeScript = this.root + '/**/*.ts';
        
        this.lessPath = this.root + '/css';
        this.lessMain = this.lessPath + '/site.less';
        this.lessWatch = [this.lessMain, this.lessPath + '/**/*.less', + this.lessPath + '/**/*.css'];

        this.typings = './tools/typings/';
        this.libraryTypeScriptDefinitions = this.typings + '**/*.ts';
        this.appTypeScriptReferences = this.typings + 'yoco.d.ts';
    }
    return GulpConfig;
})();
module.exports = GulpConfig;