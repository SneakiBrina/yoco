<?php
//Catch most errors
set_error_handler("api_errors_handler");

function api_errors_handler($errType, $errMsg, $errFile, $errLine, $errContext){
  if(ob_get_level() > 0){
    ob_clean();
  }
  
  $err = array();
  $err['type'] = $errType;
  $err['message'] = $errMsg;
  if(isset($errFile)){
    $err['file'] = $errFile;
  }
  if(isset($errLine)){
    $err['line'] = $errLine;
  }
  if(isset($errContext)){
    $err['context'] = $errContext;
  }
  
  api_error($err);
}

//Catch syntax errors
register_shutdown_function('api_fatal_syntax_error'); 

function api_fatal_syntax_error(){
	if(is_null($err = error_get_last()) === false){
    if(ob_get_level() > 0){
      ob_clean();
    }
    api_error($err);
	}
}

function api_error($err){
  global $response;
  global $API_DEBUG;
  
  $err['typeName'] = '';
  $err['fatal'] = true;

	switch ($err['type']) {
		case E_ERROR:
      $err['typeName'] = 'E_ERROR';
      break;
		case E_CORE_ERROR:
      $err['typeName'] = 'E_CORE_ERROR';
      break;
		case E_COMPILE_ERROR:
      $err['typeName'] = 'E_COMPILE_ERROR';
      break;
		case E_PARSE:
      $err['typeName'] = 'E_PARSE';
      break;
		case E_USER_ERROR:
      $err['typeName'] = 'E_USER_ERROR';
      break;
		case E_RECOVERABLE_ERROR:
      $err['typeName'] = 'E_RECOVERABLE_ERROR';
      break;
		case E_WARNING:
      $err['typeName'] = 'E_WARNING';
      $err['fatal'] = false;
      break;
		case E_CORE_WARNING:
      $err['typeName'] = 'E_CORE_WARNING';
      $err['fatal'] = false;
      break;
		case E_COMPILE_WARNING:
      $err['typeName'] = 'E_COMPILE_WARNING';
      $err['fatal'] = false;
      break;
		case E_USER_WARNING:
      $err['typeName'] = 'E_USER_WARNING';
      $err['fatal'] = false;
      break;
		case E_NOTICE:
      $err['typeName'] = 'E_NOTICE';
      $err['fatal'] = false;
      break;
		case E_USER_NOTICE:
      $err['typeName'] = 'E_USER_NOTICE';
      $err['fatal'] = false;
      break;
		case E_STRICT:
      $err['typeName'] = 'E_STRICT';
      $err['fatal'] = false;
      break;
		default:
      $err['typeName'] = 'UNKNOWN';
      $err['fatal'] = true;
      break;
	}
  
  if(!isset($API_DEBUG) || $API_DEBUG === false){
    $err['context'] = '';
    $err['file'] = '';
    $err['line'] = '';
  }
  else {
    $err['context'] = '';
    //if(isset($err['context']['sql_debug_buffer'])){
    //  $err['context']['sql_debug_buffer'] = '';
    //}
  }
  
  if($err['fatal']){
    die(json_encode(array('errors' => array($err))));
  }
  else{
    $response['errors'][] = $err;
  }
}

function handleJsonError($input){
  global $response;
  
  $err = array();
  $err['input'] = $input;
  $err['type'] = json_last_error();
  $err['name'] = '';
  $err['message'] = '';
  
  switch ($err['type']){
    case JSON_ERROR_NONE:
      $err['name'] = 'JSON_ERROR_NONE';
      $err['message'] = 'No Errors';
      break;
    case JSON_ERROR_DEPTH:
      $err['name'] = 'JSON_ERROR_DEPTH';
      $err['message'] = 'Maximum stack depth exceeded';
      break;
    case JSON_ERROR_STATE_MISMATCH:
      $err['name'] = 'JSON_ERROR_STATE_MISMATCH';
      $err['message'] = 'Underflow or the modes mismatch';
      break;
    case JSON_ERROR_CTRL_CHAR:
      $err['name'] = 'JSON_ERROR_CTRL_CHAR';
      $err['message'] = 'Unexpected control character found';
      break;
    case JSON_ERROR_SYNTAX:
      $err['name'] = 'JSON_ERROR_SYNTAX';
      $err['message'] = 'Syntax error, malformed JSON';
      break;
    case JSON_ERROR_UTF8:
      $err['name'] = 'JSON_ERROR_UTF8';
      $err['message'] = 'Malformed UTF-8 characters, possibly incorrectly encoded';
      break;
    default:
      $err['name'] = 'UNKNOWN';
      $err['message'] = 'Unknown error';
      break;
  }
  
  $response['errors'][] = $err;
  
  die(json_encode($response));
}

if(ob_get_level() > 0){
  ob_flush();
}