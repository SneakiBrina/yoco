<?php

if($response['loggedIn'] === true){
  if(isset($request['name'])){
    $response['messages'] = Chat::Messages($request['name']);
    $response['ws'] = Config::$WEB_SOCKET;
  }
}