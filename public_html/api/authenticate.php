<?php
if(isset($request['username']) && isset($request['password'])){
	if(Authentication::LogIn($request['username'], $request['password'])){
    $response['loggedIn'] = true;
		$response['accountID'] = Authentication::$accountID;
		$response['username'] = Authentication::$username;
  }
  else{
    $response['loggedIn'] = false;
  }
	$response['message'] = Authentication::$message;
}