<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

ob_start();

$endPoints = array();
$endPoints['authenticate'] = array('loggedIn' => false);
$endPoints['logout'] = array('loggedIn' => false);
$endPoints['chat'] = array();

$request = array();
$response = array();

$response['errors'] = array();
$response['loggedIn'] = false;


include_once 'api_error_handlers.php';

function add_error($typeName, $message){
  global $response;
  $response['errors'][] = array('typeName' => $typeName, 'message' => $message);
}

$requestJSON = trim(file_get_contents('php://input'));

if(strlen($requestJSON) > 0){
  $request = json_decode($requestJSON, true);
}

if($request === null){
  handleJsonError($requestJSON);
}

include_once 'sql/opensql.php';

if(isset($_COOKIE['API_DEBUG'])){
  $API_DEBUG = true;
}

foreach($endPoints as $endPoint => $data){
  if(isset($_GET[$endPoint])){
    if(isset($data['loggedIn']) && $data['loggedIn'] === true || !isset($data['loggedIn'])){
      $response['loggedIn'] = Authentication::Authenticated();
      if($response['loggedIn'] === false){
        $response['loginRequired'] = true;
      }
      else{
        include_once($endPoint . '.php');
      }
    }
    else{
      include_once($endPoint . '.php');
    }
    break;
  }
}

if(Config::API_DEBUG || isset($API_DEBUG) && $API_DEBUG === true){
  $response['sql_debug_query_count'] = $GLOBALS['sql_debug_query_count'];
  $response['sql_debug_buffer'] = $GLOBALS['sql_debug_buffer'];
}

$jsonResponse = json_encode($response);

if($jsonResponse === null){
  handleJsonError($response);
}

header('Content-Type: application/json');
echo $jsonResponse;

while (ob_get_level()){
  ob_end_flush();
}