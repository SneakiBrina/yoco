///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/yoco.d.ts" />

module yoco {
  'use strict';

  class ErrorController {
    displayHTML: string;
    jsonErrors: any;
    
	  static $inject = ['$scope', '$sce', 'errorData'];
    constructor(private $scope: ng.IScope, private $sce, private errorData) {
      if (typeof(this.errorData) === 'string') {
        this.displayHTML = this.$sce.trustAsHtml(this.errorData);
      } else {
        this.jsonErrors = this.errorData;
      }
    }
  }
  
	angular.module('yoco').controller('yoco.ErrorController', ErrorController);  
}