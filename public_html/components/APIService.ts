///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/yoco.d.ts" />

module yoco {
  'use strict';
  
  export interface APIResult {
    errors: any;
    loggedIn: boolean;
    loginRequired?: boolean;
    message?: string;
  }

  export class APIService {
    loggedIn: boolean = false;

    static $inject = ['$http', '$rootScope'];
    constructor(private $http: ng.IHttpService, private $rootScope: yoco.ApplicationScope) {
      
    }
    
    request(endpoint: string, postData: any, callback) {
      if (endpoint === 'nop') {
        return;
      }
      
      //TODO: probably shouldn't use any
      var requestCompleted: any = angular.bind(this, function(result: ng.IHttpPromiseCallbackArg<APIResult>) {
        if (result.status === 200) {
          var data = result.data;
          if (typeof(data) === 'string') {
            this.$rootScope.$broadcast('api-error', data);
          } else {
            if (data.errors.length > 0) {
              this.$rootScope.$broadcast('api-error', data);
            } else {
              if (this.$rootScope.loggedIn === false && data.loggedIn === true) {
                this.$rootScope.$broadcast('login-success');
              } else {
                if (data.loggedIn === false && data.loginRequired === true) {
                  this.$rootScope.loggedIn = false;
                  var _pendingLogin = angular.bind(this, function() {
                    this.request(endpoint, postData, callback);
                  });
                  this.$rootScope.$broadcast('pending-login', _pendingLogin);
                  this.$rootScope.$broadcast('start-login');
                  return;
                }
              }
              callback(data);
            }
          }
        }
      });
      
      this.$http.post('/api/?' + endpoint, postData)
      .then(requestCompleted);
    }
  }
  
  angular.module('yoco').service('yoco.APIService', APIService);
}