///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/yoco.d.ts" />

module yoco {
  'use strict';

  class LoginController {
    loginForm: any = {};
    username: string;
    password: string;
    rememberMe: boolean;
    messageType: string;
    message: string;
    loginInfo: any = {};
    hPassword: string = '';
  
    static $inject = ['$scope', '$rootScope', 'yoco.APIService'];
    constructor(private $scope: ng.IScope, private $rootScope: ng.IScope, private APIService) {
      this.loginInfo = localStorage.getItem('a_login');
      if (this.loginInfo !== null) {
        try {
          this.loginInfo = JSON.parse(this.loginInfo);
        } catch (e) {
          this.loginInfo = null;
        }
      }
      
      if (this.loginInfo === null) {
        this.loginInfo = {'rememberMe': false};
      }
      
      if (this.loginInfo.rememberMe) {
        this.rememberMe = true;
        this.username = this.loginInfo.username;
        this.password = this.loginInfo.password;
        this.hPassword = this.loginInfo.hPassword;
      }
    }
    
    submit() {
      this.message = '';
      if (this.loginForm.$valid) {
        if (this.hPassword === '' || this.password !== this.loginInfo.password) {
          this.hPassword = this.hashPassword(this.password);
        }
        this._login(this.username, this.hPassword);
      }
    }
    
    hashPassword(password: string) {
      return CryptoJS.SHA1(password).toString();
    }
    
    _loginCompleted(data: APIResult) {
      if (data.loggedIn) {
        if (this.rememberMe === true) {
          this.loginInfo.rememberMe = this.rememberMe;
          this.loginInfo.username = this.username;
          this.loginInfo.hPassword = this.hPassword;
          this.loginInfo.password = Array(this.password.length + 1).join('*');
          localStorage.setItem('a_login', JSON.stringify(this.loginInfo));
        }
      } else {
        this.hPassword = '';
        this.messageType = data.message.indexOf('Error:') === 0 ? 'error' : '';
        this.message = data.message.replace(/^Error:/, '');
      }
    }
    
    _login(username: string, hash: string) {
      this.APIService.request('authenticate', { username: username, password: hash }, angular.bind(this, this._loginCompleted));
    }
  }

  angular.module('yoco').controller('yoco.LoginController', LoginController);
}