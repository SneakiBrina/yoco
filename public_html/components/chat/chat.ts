///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/yoco.d.ts" />

module yoco {
  interface Language {
    languageCode: string;
    languageName: string;
  }
  
  interface ChatMessage {
    language: Language;
    message: string;
    translation: string;
    sender: string;
    time: number;
    id: number;
  }
  
  interface PageOption {
    pageName: string;
    pageID: number;
  }

  class ChatController {
    languages = [
      {
        languageCode: 'en',
        languageName: 'English'
      },
      {
        languageCode: 'de',
        languageName: 'German'
      },
      {
        languageCode: 'hu',
        languageName: 'Hungarian'
      },
      {
        languageCode: 'fr',
        languageName: 'French'
      },
      {
        languageCode: 'sv',
        languageName: 'Swedish'
      },
      {
        languageCode: 'xx-klingon',
        languageName: 'Klingon'
      }
    ];
    
    myLanguage: Language;
    
    myMessage: string;
    
    messages: Array<ChatMessage>;
    
    lastSeen: number;
    lastLoaded: number;
    
    socket: any;
    
    static $inject = ['$scope', '$modal', 'yoco.APIService', '$websocket'];
    constructor(private $scope: ng.IScope, private $modal, private APIService, private $websocket) {
      this.myLanguage = this.languages[0];
      
      this.lastSeen = 0;
      this.lastLoaded = null;
      
      this.messages = [];
      
      var localStorageLanguage = localStorage.getItem('myLanguage');
      if (localStorageLanguage !== null) {
        for (var langI = 0; langI < this.languages.length; langI++) {
          if (localStorageLanguage === this.languages[langI].languageCode) {
            this.myLanguage = this.languages[langI];
          }
        }
      }
      
      var localStorageSeen = localStorage.getItem('lastSeen');
      if (localStorageSeen !== null) {
        this.lastSeen = parseInt(localStorageSeen, 10);
      }
      
      this.$scope.$watch(() => this.myLanguage,
                               (_new: Language, _old: Language) => {
        this.languageChanged(_old, _new);
      });
      
      var _chatMessages: any = angular.bind(this, this.chatMessages);
      this.APIService.request('chat', {
        'name': 'All'
      }, _chatMessages);
    }
    
    private languageChanged(_old: Language, _new: Language) {
      if (_old !== null && _old.languageCode !== _new.languageCode) {
        localStorage.setItem('myLanguage', _new.languageCode);
      }
    }
    
    private chatMessages(data: any) {
      this.messages.splice(0, this.messages.length);
      this.messages.push.apply(this.messages, data.messages);
      
      if (this.messages.length > 0) {
        this.lastSeen = this.messages[this.messages.length - 1].id;
      }
      
      this.socket = this.$websocket.$new(data.ws);
      
      this.socket.$on('$open', function() {
        this.socket.$emit('lastSeen', this.lastSeen);
      });
      
      this.socket.$on('msg', this.onMessage);
    }
    
    private onMessage(json) {
      var data = JSON.parse(json);
      this.messages.push(data);
    }
    
    public send() {
      //TODO make sure websocket is connected
      this.socket.$emit('msg', {
        'message': this.myMessage,
        'languageCode': this.myLanguage.languageCode
      });
      this.myMessage = '';
    }
  }

  angular.module('yoco').controller('yoco.ChatController', ChatController);
}