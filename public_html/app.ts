///<reference path="../tools/typings/tsd.d.ts" />
///<reference path="../tools/typings/yoco.d.ts" />

module yoco {
  'use strict';

  var _app = angular.module('yoco', [
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'ngSanitize',
    'ngWebsocket'
  ]);

  _app.config(['$routeProvider',
    function ($routeProvider: ng.route.IRouteProvider) : void {
      $routeProvider
        .when('/chat', {
          controller: 'yoco.ChatController',
          templateUrl: 'components/chat/chat.html',
          controllerAs: 'chat'
        })
        .otherwise({redirectTo: '/chat'});
    }
  ])
  .config(['$compileProvider',
    function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|data):/);
  }]);
  
  export interface ApplicationScope extends ng.IScope {
    firstLogin: boolean;
    loggedIn: boolean;
  }
  
  class AppController {
    loginModal: any = false;
    errorModal: any = false;
    pendingCallbacks: Array<any> = [];
    
    static $inject = ['$scope', '$rootScope', '$modal', 'yoco.APIService'];
    constructor(private $scope: ng.IScope, private $rootScope: ApplicationScope, private $modal, private APIService) {

      var _startLogin: any = angular.bind(this, this.startLogin);
      $rootScope.$on('start-login', _startLogin);
      
      var _loginSuccess: any = angular.bind(this, this.loginSuccess);
      $rootScope.$on('login-success', _loginSuccess);
      
      var _loggedOut: any = angular.bind(this, this.loggedOut);
      $rootScope.$on('logged-out', _loggedOut);
      
      var _pendingLogin: any = angular.bind(this, this.pendingLogin);
      $rootScope.$on('pending-login', _pendingLogin);
      
      var _apiError: any = angular.bind(this, this.apiError);
      $rootScope.$on('api-error', _apiError);
      
      var _jsError: any = angular.bind(this, this.jsError);
      $rootScope.$on('js-error', _jsError);
    }
    
    jsError(event: any, error: any) {
      console.log(error);
    }
    
    apiError(event: any, errorData: any) {
      this.errorModal = this.$modal.open({
        templateUrl: 'components/error/error.html',
        controller: 'yoco.ErrorController as error',
        resolve: {
          errorData: angular.bind(this, function() {
            return errorData;
          })
        },
        windowClass: 'modal-error'
      });
    }
    
    pendingLogin(event: any, callback: any) {
      if (this.$rootScope.loggedIn === false) {
        this.pendingCallbacks.push(callback);
      } else {
        callback();
      }
    }
    
    startLogin() {
      if (this.loginModal === false) {
        this.loginModal = this.$modal.open({
          templateUrl: 'components/login/login.html',
          controller: 'yoco.LoginController as login',
          backdrop: 'static',
          keyboard: false,
          windowClass: 'modal-login'
        });
      }
    }
    
    loginSuccess() {
      this.$rootScope.firstLogin = false;
      this.$rootScope.loggedIn = true;
      if (this.loginModal !== false) {
        this.loginModal.close();
      }
      this.loginModal = false;
      
      var callback = this.pendingCallbacks.shift();
      while (typeof(callback) !== 'undefined') {
        callback();
        callback = this.pendingCallbacks.shift();
      }
    }
    
    loggedOut() {
      this.$rootScope.loggedIn = false;
    }
  }
  
  _app.controller('yoco.AppController', AppController);
  
  _app.run(['$rootScope', function($rootScope) {
    $rootScope.loggedIn = false;
    $rootScope.firstLogin = true;
  }]);
}