<?php
include '../config/current.config';

if(Config::SSL_ENABLED && $_SERVER['HTTPS'] != 'on'){
  header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  exit();
}

function buildFileUrl($uri){
  $realPath = realPath($_SERVER['DOCUMENT_ROOT'] . $uri);
  if($realPath !== false){
    $ftime = filemtime($realPath);
    if($ftime !== false){
      $uri .= '?' . $ftime;
    }
  }
  return $uri;
}
?>
<!DOCTYPE html>
<html data-ng-app='yoco'>
<head>
  <title data-ng-bind='pageTitle'>Yoco</title>
  
  <link href='bower_components/bootstrap/dist/css/bootstrap.css' rel='stylesheet' />
  <link rel='stylesheet' href='<?php echo buildFileUrl('/css/site.css'); ?>' type='text/css' />
  
  <script src='bower_components/jquery/dist/jquery.min.js' type='text/javascript' ></script>
  <script src='bower_components/jquery-ui/jquery-ui.min.js' type='text/javascript' ></script>
  <script src='bower_components/angular/angular.js' type='text/javascript' ></script>
</head>

<body class='yoco' data-ng-cloak data-ng-controller='yoco.AppController as app'>
  <div id='content' data-ng-view></div>
  
  <script src="bower_components/ng-websocket/ng-websocket.js"></script>
  <script src='bower_components/angular-sanitize/angular-sanitize.min.js' type='text/javascript' ></script>
  <script src='bower_components/angular-route/angular-route.min.js' type='text/javascript' ></script>
  <script src='bower_components/angular-animate/angular-animate.min.js' type='text/javascript' ></script>
  <script src='bower_components/angular-bootstrap/ui-bootstrap.min.js' type='text/javascript' ></script>
  <script src='bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js' type='text/javascript' ></script>
  <script src='bower_components/cryptojs/lib/Crypto.js' type='text/javascript' ></script>
  <script src='bower_components/cryptojs/lib/SHA1.js' type='text/javascript' ></script>
  
  <script type='text/javascript'>
    //TODO: Hack
    var CryptoJS = Crypto;
  </script>
  
  <script src='<?php echo buildFileUrl('/app.js'); ?>' type='text/javascript'></script>
  <script src='<?php echo buildFileUrl('/components/APIService.js'); ?>' type='text/javascript'></script>
  <script src='<?php echo buildFileUrl('/components/login/login.js'); ?>' type='text/javascript'></script>
  <script src='<?php echo buildFileUrl('/components/chat/chat.js'); ?>' type='text/javascript'></script>
  
  <script src='<?php echo buildFileUrl('/components/error/error.js'); ?>' type='text/javascript'></script>
  
  <!--<script src='<\?php echo buildFileUrl('/admin/directives/dropzone.js'); ?>' type='text/javascript'></script>!-->
</body>
</html>