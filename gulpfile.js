'use strict';

var gulp = require('gulp'),
  debug = require('gulp-debug'),
  inject = require('gulp-inject'),
  tsc = require('gulp-typescript'),
  tslint = require('gulp-tslint'),
  sourcemaps = require('gulp-sourcemaps'),
  del = require('del'),
  Config = require('./gulpfile.config'),
  less = require('gulp-less'),
  gutil = require('gulp-util');
  //livereload = require('gulp-livereload');

var config = new Config();

/**
 * Generates the app.d.ts references file dynamically from all application *.ts files.
 */
gulp.task('gen-ts-refs', function () {
  var target = gulp.src(config.appTypeScriptReferences);
  var sources = gulp.src([config.allTypeScript], {read: false});
  return target.pipe(inject(sources, {
    starttag: '//{',
    endtag: '//}',
    transform: function (filepath) {
      return '/// <reference path="..' + filepath + '" />';
    }
  })).pipe(gulp.dest(config.typings));
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('ts-lint', function () {
  return gulp.src(config.allTypeScript).pipe(tslint()).pipe(tslint.report('prose'));
});

/**
 * Compile TypeScript and include references to library and app .d.ts files.
 */
gulp.task('compile-ts', function () {
  var sourceTsFiles = [
    config.allTypeScript,                //path to typescript files
    config.libraryTypeScriptDefinitions, //reference to library .d.ts files
    config.appTypeScriptReferences
  ];     //reference to app.d.ts files

  var tsResult = gulp.src(sourceTsFiles)
    .pipe(sourcemaps.init())
    .pipe(tsc({
      module: 'commonjs',
      target: 'ES5',
      declarationFiles: false,
      noExternalResolve: true,
      experimentalDecorators: true
    }));

  tsResult.dts.pipe(gulp.dest(config.tsOutputPath));
  return tsResult.js
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.tsOutputPath));
});

gulp.task('less-site', function(){
  gulp.src(config.lessMain)
    .pipe(less())
    .on('error', gutil.log)
    .pipe(gulp.dest(config.lessPath));
    //.pipe(livereload());
});

gulp.task('watch', function() {
  gulp.watch([config.allTypeScript], ['ts-lint','compile-ts', 'gen-ts-refs']);
  gulp.watch(config.lessWatch, ['less-site']);
});

gulp.task('default', ['less-site','ts-lint','compile-ts', 'gen-ts-refs', 'watch']);