<?php
//Catch most errors
set_error_handler("php_errors_handler");

function php_friendly_error_type($level){
	switch ($level) {
		case E_ERROR:
			return 'E_ERROR';
		case E_CORE_ERROR:
			return 'E_CORE_ERROR';
		case E_COMPILE_ERROR:
			return 'E_COMPILE_ERROR';
		case E_PARSE:
			return 'E_PARSE';
		case E_USER_ERROR:
			return 'E_USER_ERROR';
		case E_RECOVERABLE_ERROR:
			return 'E_RECOVERABLE_ERROR';
			
		case E_WARNING:
			return 'E_WARNING';
		case E_CORE_WARNING:
			return 'E_CORE_WARNING';
		case E_COMPILE_WARNING:
			return 'E_COMPILE_WARNING';
		case E_USER_WARNING:
			return 'E_USER_WARNING';
		case E_NOTICE:
			return 'E_NOTICE';
		case E_USER_NOTICE:
			return 'E_USER_NOTICE';
		case E_STRICT:
			return 'E_STRICT';
		default:
			return 'UNKNOWN_' . $level;
	}
}

function php_errors_handler($error_level, $error_message, $error_file, $error_line, $error_context){
	switch ($error_level) {
		case E_ERROR:
		case E_CORE_ERROR:
		case E_COMPILE_ERROR:
		case E_PARSE:
		case E_USER_ERROR:
		case E_RECOVERABLE_ERROR:
			$fatal = true;
			break;
		case E_WARNING:
		case E_CORE_WARNING:
		case E_COMPILE_WARNING:
		case E_USER_WARNING:
		case E_NOTICE:
		case E_USER_NOTICE:
		case E_STRICT:
		default:
			$fatal = false;
			break;
	}
	
	if($fatal){
		$minLoad = true;
	}
	require_once("opensql.php");
	//echo "$error_message\n$error_file:$error_line";
	$errorClassName = "Error";
	$phpError = new $errorClassName(php_friendly_error_type($error_level) . ": $error_message\n$error_file:$error_line","");
	$phpError->submit();
	
	if($fatal){
		$phpError->fatal();
	}
	
	return true;
}

//Catch syntax errors
register_shutdown_function('fatal_syntax_error'); 

function fatal_syntax_error(){ 
	if(is_null($e = error_get_last()) === false){ 
		$minLoad = true;
		require_once("opensql.php");
		
		$errorClassName = "Error";
		$syntaxError = new $errorClassName(php_friendly_error_type($e["type"]) . ": " . $e["message"] . "\n" .$e["file"] . ":" . $e["line"],"");
		$syntaxError->submit();
	}
}