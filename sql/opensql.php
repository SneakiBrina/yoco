<?php
include_once 'error_handlers.php';

include_once 'config/current.config';

$GLOBALS['sql_debug'] = (isset($_GET['vancerocks'])? $_GET['vancerocks'] : Config::DEBUG_MODE);
$GLOBALS['sql_debug_buffer'] = '';
$GLOBALS['sql_debug_query_count'] = 0;

class SQL extends PDO{
	public static $Link = false;
	
	//from http://www.php.net/manual/en/pdo.connections.php
	public static function exception_handler($exception){
		SQL::fatalError('Error Initializing PDO', $exception->getMessage());
	}
	
	private static function fatalError($type, $message){
		$rc = new ReflectionClass('Error');
		$error = $rc->newInstanceArgs( array($type, $message) );
		$error->noDB(true);
		$error->forceEmail(true);
		$error->submit();
		$error->display();
		$error->fatal();
	}

	public function __construct($dsn, $username='', $password='', $driver_options=array(), $safe=false){
		if ($safe){
			// Temporarily change the PHP exception handler while we . . .
			set_exception_handler(array(__CLASS__, 'exception_handler'));
		}

		// . . . create a PDO object
		parent::__construct($dsn, $username, $password, $driver_options);

		if ($safe){
			// Change the exception handler back to whatever it was before
			restore_exception_handler();
		}
	}
	
	public static function DB($persistent=true){
		if($persistent === false){
			return SQL::Connect($persistent);
		}
		
		if (!SQL::$Link){
			SQL::$Link = SQL::Connect($persistent);
		}
		return SQL::$Link;
	}
	
	public static function Connect($persistent=true){
		$options = array(PDO::ATTR_PERSISTENT => $persistent,
										 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
										 PDO::ATTR_EMULATE_PREPARES => false,
										 PDO::ATTR_STRINGIFY_FETCHES => true);
		
		try{
			$link = new SQL('mysql:host=' . Config::SQL_HOST . ';dbname=' . Config::SQL_DB,
				Config::SQL_USER, Config::SQL_PASSWORD, $options, true);
		}
		catch (PDOException $e){
			SQL::fatalError('Error connecting to database', $e->getMessage());
		}
		
		return $link;
	}
	
	public static function Error($errorMessage, $stmt, $noDB = false){
		$rc = new ReflectionClass('Error');
		$comment = '';
		
		if($stmt != false && $stmt->fullQuery != null){
			$comment = $stmt->fullQuery;
		}
		
		$error = $rc->newInstanceArgs( array($errorMessage, $comment ));
		if($noDB){
			$error->noDB();
		}
		$error->forceEmail();
		$error->submit();
		$error->display();
		$error->fatal();
		die();
	}
	
	public function execute($stmt){
		$result = $stmt->execute();
		if($GLOBALS['sql_debug'] >= 2){
			$GLOBALS['sql_debug_query_count'] += 1;
			$GLOBALS['sql_debug_buffer'] .= $stmt->fullQuery . ";\n\n";
		}
		return $result;
	}
	
	public static function GetError($stmt){
		return $stmt->errorInfo()[2];
	}
	
	public static function SafeInt($content){
		return filter_var($content, FILTER_VALIDATE_INT);
	}
	
	public static function SafeFloat($content){
		return filter_var($content, FILTER_VALIDATE_FLOAT);
	}
	
	public static function Safe($content, $addQuotes=true){
		$safe = SQL::quote($content);
		if ($addQuotes == false){
			$safe = substr($safe, 1, count($safe)-2);
		}
		return $safe;
	}
	
	public function prepare($statement, $options = array()){
		$options[PDO::ATTR_STATEMENT_CLASS] = array('SQLStatement', array($this));
		return parent::prepare($statement, $options);
	}
}

class SQLStatement extends PDOStatement{
	protected function __construct($sql){
		if($sql instanceof SQL){
			$this->_sql = $sql;
		}
	}
	
	protected $_sql = '';
	
	public $fullQuery;
	
	protected $params = array();
	
	public function bindParam($param, &$value, $datatype = PDO::PARAM_STR, $length = 0, $driverOptions = false){
		$this->params[$param] = &$value;
		return parent::bindParam($param, $value, $datatype, $length, $driverOptions);
	}
	
	public function bindValue($param, $value, $datatype = PDO::PARAM_STR){
		$this->params[$param] = $value;
		return parent::bindValue($param, $value, $datatype);
	}
	
	public function resolveQuery($inputParams = null){
		$testQuery = $this->queryString;
		
		if($this->params){
			ksort($this->params);
			foreach($this->params as $key => $array){
				if(is_numeric($key)){
					$key = '\?';
				}
				else{
					$key = (preg_match('/^\:/', $key))? $key: ':' . $key;
				}
				$value = $array;
				$testParam = '/' . $key . '(?!\w)/';
				$replValue = $this->_prepareValue($value);
				$testQuery = preg_replace($testParam, $replValue, $testQuery);
				}
			}
			
			if(is_array($inputParams) && $inputParams !== array()){
				ksort($inputParams);
				foreach($inputParams as $key => $replValue){
				if(is_numeric($key)){
					$key = '\?';
				}
				else{
					$key = (preg_match('/^\:/', $key))? $key: ':' . $key;
				}
				$testParam = '/' . $key . '(?!\w)/';
				$replValue = $this->_prepareValue($replValue);
				$testQuery = preg_replace($testParam, $replValue, $testQuery);
				}
			}
			$this->fullQuery = $testQuery;
			return $testQuery;
		}
		
    public function execute($inputParams = null){
			$this->resolveQuery($inputParams);
      parent::setFetchMode(PDO::FETCH_ASSOC);
			return parent::execute($inputParams);
		}
		
		private function _prepareValue($value){
			if($this->_sql && ($this->_sql instanceof SQL)){
				$value = $this->_sql->quote($value);
			}
			else{
				$value = "'" . addslashes($value) . "'";
			}
			return $value;
		}
}

include 'sql/functions/init.php';
?>