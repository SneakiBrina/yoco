<?php
class Error{
	private $comment;
	private $errorMessage;
	private $backtrace = '';
	
	private $noDatabase = false;
	private $mustEmail = false;
	
	public function __construct($errorMessage, $comment, $showBacktrace=true){
		$this->comment = $comment;
		$this->errorMessage = $errorMessage;
    
    if($showBacktrace){
      $backtrace = debug_backtrace();
      
      for($i = 0; $i < count($backtrace); $i++){
        if(isset($backtrace[$i]['file']) && isset($backtrace[$i]['line'])){ //TODO is this right ?
          $this->backtrace = 'at ' . $backtrace[$i]['file'] . ':' . $backtrace[$i]['line'] . "\r\n";
          break;
        }
      }
      
      for(; $i < count($backtrace); $i++){
        $this->backtrace .= 'in ';
        if(isset($backtrace[$i]['function'])){
          $this->backtrace .= $backtrace[$i]['function'];
        }
        else{
          $this->backtrace .= 'UNKNOWN_FUNCTION';
        }
        $this->backtrace .= '(';
        
        if(isset($backtrace[$i]['args'])){
          ob_start();
          for($argI = 0; $argI < count($backtrace[$i]['args']); $argI++){
            if($argI != 0){
              echo ',';
            }
            var_dump($backtrace[$i]['args'][$argI]);
          }
          $this->backtrace .= ob_get_clean();
        }
        $this->backtrace .= ') ';
        if(isset($backtrace[$i]['file'])){
          $this->backtrace .= $backtrace[$i]['file'];
        }
        else{
          $this->backtrace .= 'UNKNOWN_FILE';
        }
        $this->backtrace .= ':';
        if(isset($backtrace[$i]['line'])){
          $this->backtrace .= $backtrace[$i]['line'];
        }
        else{
          $this->backtrace .= 'UNKNOWN_LINE';
        }
        $this->backtrace .= "\r\n";
      }
      $this->backtrace .= 'in ' . $_SERVER['PHP_SELF'];
    }
	}
	
	public function noDB(){
		$this->noDatabase = true;
	}
	
	public function forceEmail(){
		$this->mustEmail = true;
	}
	
	public function fatal(){
		die();
	}
	
	public function submit(){
		$wantsToSend = false;
		
		if(!$this->noDatabase){
			$db = SQL::DB();
			$stmt = false;
		
			try{
				$stmt = $db->prepare('SELECT count(*) FROM errors
WHERE time > NOW()-60*60 AND seen = 0 AND reported = 1 AND message = :message');
				$stmt->bindParam(':message', $this->errorMessage, PDO::PARAM_STR);
				
				if(!$db->execute($stmt)){
					$dbError = new Error('Could not read error reported count from database', SQL::GetError($stmt));
					$dbError->noDB();
					$dbError->forceEmail();
					$dbError->submit();
				
					$this->noDB();
				}
				else{
					$wantsToSend = current($stmt->fetch()) == 0;
					
					$stmt = $db->prepare('SELECT errorID FROM errors
WHERE time > NOW()-' . Errors::$MERGE_TIME . ' AND message = :message
ORDER BY errorID asc
LIMIT 1');
					$stmt->bindParam(':message', $this->errorMessage, PDO::PARAM_STR);

					if(!$db->execute($stmt)){
						$dbError = new Error('Could not read error count from database', SQL::GetError($stmt));
						$dbError->noDB();
						$dbError->forceEmail();
						$dbError->submit();
						
						$this->noDB();
					}
					else {
						$data = $stmt->fetchAll();
						
						if(count($data) == 0){
							$stmt = $db->prepare('INSERT INTO errors
(time, message, comment, backtrace, seen, times, reported)
VALUE(NOW(), :message, :comment, :backtrace, 0, 1, :reported)');
							
							$stmt->bindParam(':message', $this->errorMessage, PDO::PARAM_STR);
							$stmt->bindParam(':comment', $this->comment, PDO::PARAM_STR);
							$stmt->bindParam(':backtrace', $this->backtrace, PDO::PARAM_STR);
							$stmt->bindValue(':reported', $wantsToSend? 1 : 0, PDO::PARAM_INT);
							
							if(!$db->execute($stmt)){
								$dbError = new Error('Could not save error count to database', SQL::GetError($stmt));
								$dbError->noDB();
								$dbError->forceEmail();
								$dbError->submit();
								
								$this->noDB();
							}
						}
						else{
							$stmt = $db->prepare('UPDATE errors
SET times = times + 1
WHERE errorID= :errorID');
							$stmt->bindParam(':errorID', $data[0]["errorID"], PDO::PARAM_INT);
							if(!$db->execute($stmt)){
								$dbError = new Error('Could not increment error count in database', SQL::GetError($stmt));
								$dbError->noDB();
								$dbError->forceEmail();
								$dbError->submit();
								
								$this->noDB();
							}
						}
					}
				}
			}
			catch (PDOException $e){
				SQL::Error($e->getMessage(), $stmt, true);
			}
		}
		
		if($this->noDatabase){
			$fileName = Config::ERROR_DIR . substr(preg_replace('/[^a-zA-Z]*/', '', $this->errorMessage), 0, 100) . '.txt';
			if(file_exists($fileName)){
				$this->mustEmail = false;
			}
			else{
				$this->mustEmail = true;
			}
			file_put_contents($fileName, 'ERROR(' . time() . '): ' . $this->errorMessage . " '" . $this->comment . "' \r\n" . $this->backtrace . "\r\n\r\n", FILE_APPEND | LOCK_EX);
		}
		
		if(($this->mustEmail && $this->noDatabase) || (Settings::Get('ERROR_EMAILS') == 'ON' && $wantsToSend)){
			$email = new Email('No Reply', 'Error', 'Error' . ($this->noDatabase? '(OFFLINE): ' : ': ') . $this->errorMessage);
			$email->text('ERROR: ' . $this->errorMessage . " '" . $this->comment . "' \r\n" . $this->backtrace);
			$email->send($this->mustEmail);
		}
	}
	
	public function display(){
		$error = ($GLOBALS['sql_debug'] > 0)? 'ERROR: ' . $this->errorMessage . " '" . $this->comment . "' \r\n<br />" . str_replace("\r\n", '<br />', $this->backtrace) : '';
		include 'errors/somethingwentwrong.php';
	}
}

class Errors{
	public static $MERGE_TIME = 60; //One Minute;
	
	public static function Recent($limit){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT *
FROM errors
ORDER BY errorID DESC
LIMIT :limit');
			$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
			
			if($db->execute($stmt)){
				return $stmt->fetchAll();
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
	
	public static function Seen($errorID){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('UPDATE errors
SET seen=1
WHERE errorID=:errorID');
			$stmt->bindParam(':errorID', $errorID, PDO::PARAM_INT);
			
			return $db->execute($stmt);
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
}