<?php
class Settings{
	private static $cache = array();
	
	public static function Get($settingName){
		if(isset($cache[$settingName])){
			return $cache[$settingName];
		}
		else{
			$db = SQL::DB();
			$stmt = false;
		
			try{
				$stmt = $db->prepare('SELECT settingValue
FROM settings
WHERE settingName=:settingName');
				$stmt->bindParam(':settingName', $settingName, PDO::PARAM_STR);
				
				if($db->execute($stmt)){
					if($data = $stmt->fetch()){
						$cache[$settingName] = $data['settingValue'];
						return $data['settingValue'];
					}
				}
			}
			catch (PDOException $e){
				SQL::Error($e->getMessage(), $stmt);
			}
		}
		return false;
	}
	
	public static function Set($settingName, $settingValue){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('UPDATE settings
SET settingValue=:settingValue
WHERE settingName=:settingName');
			$stmt->bindParam(':settingValue', $settingValue, PDO::PARAM_STR);
			$stmt->bindParam(':settingName', $settingName, PDO::PARAM_STR);
			
			return $db->execute($stmt);
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
}
?>