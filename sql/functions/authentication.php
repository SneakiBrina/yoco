<?php
class Authentication{
	public static $LOGIN_WINDOW = 1800;//60*30
	public static $accountID = null;
	public static $username = null;
	public static $loggedIn = false;
	public static $message = null;
	public static $ticket = null;

	private static function GenerateHash($password){
		$salt = md5(uniqid(rand(), true));
		return crypt($password, '$6$rounds=45489$' . $salt. '$');
	}

	private static function CheckHash($password, $hash){
		return (crypt($password, $hash) === $hash);
	}

	private static function BrowserSignature(){
		$data = $_SERVER['HTTP_HOST'];
		$data .= $_SERVER['HTTP_USER_AGENT'];
		return sha1($data);
	}

	private static function NewTicket($accountID, $passwordHash){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			do{
				$newTicket = sha1(uniqid('',true) . Authentication::BrowserSignature() . $passwordHash);
				
				$stmt = $db->prepare('SELECT count(*)
FROM tickets
WHERE ticket=:ticket');
				$stmt->bindParam(':ticket', $newTicket, PDO::PARAM_STR);
				
				if(!$db->execute($stmt)){
					return false;
				}
			}
			while(current($stmt->fetch()) > 0);
		
			if(Authentication::$loggedIn && isset($_COOKIE['ticket'])){
				$stmt = $db->prepare('UPDATE tickets
SET ticket=:newTicket, lastActivity=:time
WHERE ticket=:ticket');
				$stmt->bindParam(':newTicket', $newTicket, PDO::PARAM_STR);
				$stmt->bindValue(':time', time(), PDO::PARAM_INT);
				$stmt->bindParam(':ticket', $_COOKIE['ticket'], PDO::PARAM_STR);
				
				if(!$db->execute($stmt)){
					return false;
				}
			}
			else{
				$stmt = $db->prepare('INSERT INTO tickets
(ticket,clientHash,lastActivity,accountID)
VALUES(:newTicket, :signature, :time, :accountID)');
				$stmt->bindParam(':newTicket', $newTicket, PDO::PARAM_STR);
				$stmt->bindValue(':time', time(), PDO::PARAM_INT);
				$stmt->bindValue(':signature', Authentication::BrowserSignature(), PDO::PARAM_STR);
				$stmt->bindParam(':accountID', $accountID, PDO::PARAM_INT);
				
				if(!$db->execute($stmt)){
					return false;
				}
			}
			
			Authentication::$ticket = $newTicket;
			setcookie('ticket',$newTicket,$_SERVER['REQUEST_TIME'] + Authentication::$LOGIN_WINDOW, '/');
			return true;
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}

	private static function CleanTickets(){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('DELETE FROM tickets
WHERE lastActivity < :time');
			$stmt->bindValue(':time', time() - Authentication::$LOGIN_WINDOW, PDO::PARAM_INT);
			
			return $db->execute($stmt);
		}
		catch(PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}

	public static function Logout(){
		if(isset($_COOKIE['ticket'])){
			$db = SQL::DB();
			$stmt = false;
		
			try{
				$stmt = $db->prepare('DELETE FROM tickets
WHERE ticket=:ticket');
				$stmt->bindParam(':ticket', $_COOKIE['ticket'], PDO::PARAM_STR);
				$db->execute($stmt);
			}
			catch(PDOException $e){
				SQL::Error($e->getMessage(), $stmt);
			}
		}
		setcookie('ticket','expired',time()-60*60*24,'/');
		
		Authentication::$accountID = null;
		Authentication::$username = null;
		Authentication::$loggedIn = false;
		Authentication::$message = 'You are now logged out';
		Authentication::$ticket = null;
	}

	private static function LogAttempt($user,$result){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('INSERT INTO loginAttempts
(`time`,`user`,`result`,`ip`,`useragent`)
VALUES (NOW(),:user,:result,:ip,:useragent)');
			$stmt->bindParam(':user', $user, PDO::PARAM_STR);
			$stmt->bindParam(':result', $result, PDO::PARAM_INT);
			$stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
			$stmt->bindValue(':useragent', $_SERVER['HTTP_USER_AGENT'], PDO::PARAM_STR);
			
			return $db->execute($stmt);
		}
		catch(PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
	
	private static function CheckHTTPS(){
		if(Config::SSL_ENABLED && $_SERVER['HTTPS'] != 'on'){
			header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			exit();
		}
	}
	
	public static function LogIn($user,$passwordHash){
		Authentication::CheckHTTPS();
		
		if(isset($_COOKIE['ticket'])){
			Authentication::Logout();
		}
		
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT accountID, password
FROM accounts
WHERE username=:user');
			$stmt->bindParam(':user', $user, PDO::PARAM_STR);
			
			if($db->execute($stmt)){
				if($data = $stmt->fetch()){
          if($data['password'] === 'notset'){
            $stmt = $db->prepare('UPDATE accounts
SET password=:hash
WHERE accountID=:id');
						$stmt->bindValue(':hash', Authentication::GenerateHash($passwordHash), PDO::PARAM_STR);
						$stmt->bindParam(':id', $data["accountID"], PDO::PARAM_INT);
						
						if(!$db->execute($stmt)){
							return false;
						}
            
            $data['password'] = Authentication::GenerateHash($passwordHash);
          }
          
					if ($data['password'] != '' && Authentication::CheckHash($passwordHash, $data['password'])){
						Authentication::NewTicket($data['accountID'], $passwordHash);
						Authentication::$loggedIn = true;
						Authentication::$accountID = $data['accountID'];
						Authentication::$username = $user;
						Authentication::$message = 'Login success';
						Authentication::CleanTickets();
						
						Authentication::LogAttempt($user,0); //Success
						return true;
					}
					else{
						Authentication::$message = 'Error: Incorrect username or password';
						Authentication::LogAttempt($user,1); //Bad Password or User
            return false;
					}
				}
        else {
          Authentication::$message = 'Error: Incorrect username or password';
          Authentication::LogAttempt($user,1); //Bad Password or User
          return false;
        }
			}
		}
		catch(PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		Authentication::$message = 'Error: Bad login attempt, please contact administrator if this continues.';
		Authentication::LogAttempt($user,2); //Bad attempt or fatal error
		return false;
	}
	
	public static function Authenticated(){
		Authentication::CheckHTTPS();
		
		if(Authentication::$loggedIn){
			return true;
		}
		else{
			if(isset($_COOKIE['ticket'])){
				$db = SQL::DB();
				$stmt = false;
			
				try{
					$stmt = $db->prepare('SELECT accounts.accountID, username, password, clientHash, lastActivity
FROM tickets
JOIN accounts on tickets.accountID = accounts.accountID
WHERE tickets.ticket=:ticket');
					$stmt->bindParam(':ticket', $_COOKIE['ticket'], PDO::PARAM_STR);
					
					if($db->execute($stmt)){
						if($data = $stmt->fetch()){
							if($data['lastActivity'] > (time() - Authentication::$LOGIN_WINDOW)){
								if($data['clientHash'] != Authentication::BrowserSignature()){
									$error = new Error('Ticket matched but Sig didn\'t', 'AccountID: ' . $data['accountID'] . ' IP: ' . $_SERVER['REMOTE_ADDR'] . ' Referer: ' . @$_SERVER['HTTP_REFERER'] . ' User Agent: ' . $_SERVER['HTTP_USER_AGENT'] . ' Ticket: ' . $_COOKIE['ticket'] . ' Sig: ' . Authentication::BrowserSignature());
									$error->submit();
									
									Authentication::Logout();
									
									Authentication::$message = 'Error: Logged out cause client hash does not match';
									return false;
								}
								else{
									Authentication::NewTicket($data['accountID'], $data['password']);
									Authentication::$loggedIn = true;
									Authentication::$accountID = $data['accountID'];
									Authentication::$username = $data['username'];
									Authentication::$message = 'Ticket matches, You are still logged in.';
									return true;
								}
							}
							else{
								Authentication::Logout();
								Authentication::$message = 'You have been auto logged out due to inactivity.';
								return false;
							}
						}
					}
				}
				catch(PDOException $e){
					SQL::Error($e->getMessage(), $stmt);
				}
				return false;
			}
		}
		return false;
	}
	
	//TODO:password requirements ?
	public static function ChangePassword($oldPassword, $newPassword){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT accounts.accountID, username, password, clientHash, lastActivity
FROM tickets
JOIN accounts on tickets.accountID = accounts.accountID
WHERE tickets.ticket=:ticket');
			$stmt->bindParam(':ticket', Authentication::$ticket);
			
			if($db->execute($stmt)){
				if($data = $stmt->fetch()){
					if(Authentication::CheckHash(oldPassword, $data["password"])){
					
						$stmt = $db->prepare('UPDATE accounts
SET password=:hash
WHERE accountID=:id');
						$stmt->bindValue(':hash', Authentication::GenerateHash($newPassword), PDO::PARAM_STR);
						$stmt->bindParam(':id', $data["accountID"], PDO::PARAM_INT);
						
						if($db->execute($stmt)){
							return true;
						}
					}
				}
			}
		}
		catch(PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
	
	public static function RecentLoginAttempts($count){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT *
FROM loginAttempts
ORDER BY attemptID DESC
LIMIT :limit');
			$stmt->bindParam(':limit', $count, PDO::PARAM_INT);
			
			if($db->execute($stmt)){
				return $stmt->fetchAll();
			}
		}
		catch(PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
}
?>