<?php
class Email{
	private $contentType = null;
	private $from = null;
	private $replyTo = null;
	private $to = null;
	private $subject = null;
	private $content = null;
	private $headers = null;
	private $emailID = null;
	
	public function __construct($from, $to, $subject){
		if(is_array($from)){
			$this->from = isset(Config::$EMAIL_ADDRESSES[$from[0]])? Config::$EMAIL_ADDRESSES[$from[0]] : $from[0];
			$this->replyTo = isset(Config::$EMAIL_ADDRESSES[$from[1]])? Config::$EMAIL_ADDRESSES[$from[1]] : $from[1];
		}
		else{
			$this->from = $this->replyTo = isset(Config::$EMAIL_ADDRESSES[$from])? Config::$EMAIL_ADDRESSES[$from] : $from;
		}
		
		$this->to = isset(Config::$EMAIL_ADDRESSES[$to])? Config::$EMAIL_ADDRESSES[$to] : $to;
		$this->subject = $subject;
	}
	
	public function html($contents){
		$this->contentType = 'text/html';
		$this->content = $contents;
	}
	
	public function text($contents){
		$this->contentType = 'text/plain';
		$this->content = $contents;
	}
	
	private function prepare(){
		$this->headers = "From: " . $this->from . "\r\nReply-To: " . $this->replyTo . "\r\nReturn-Path: " . $this->from . "\r\nContent-Type: " . $this->contentType . "\r\n";
	}
	
	public function queue(){
		$this->prepare();
		
		$db = SQL::DB(false);
		$stmt = false;
		
		try{	
			$stmt = $db->prepare('INSERT INTO emails
(`to`, `from`, `subject`, `headers`, `content`, `timeQueued`)
VALUES(:to, :from, :subject, :headers, :content, NOW())');
			
			$stmt->bindParam(':to', $this->to, PDO::PARAM_STR);
			$stmt->bindParam(':from', $this->from, PDO::PARAM_STR);
			$stmt->bindParam(':subject', $this->subject,  PDO::PARAM_STR);
			$stmt->bindParam(':headers', $this->headers,  PDO::PARAM_STR);
			$stmt->bindParam(':content', $this->content,  PDO::PARAM_STR);

			if($db->execute($stmt)){
				$this->emailID = $db->lastInsertId();
				return true;
			}
			else{
				$error = new Error('Could not queue email to database', SQL::GetError($stmt));
				$error->noDB();
				$error->forceEmail();
				$error->submit();
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		
		return false;
	}
	
	public function send($force=false){
		if ($this->queue() === false && !$force){
			return false;
		}
		
		$canSend = Emails::CanSend();
		if($force || $canSend === true){
			$result = mail($this->to, $this->subject, $this->content, $this->headers);
			
			if($result){
				$this->sent();
				return true;
			}else{
				$this->failed('mail returned false, not sent.');
			}
		}else{
			$this->failed($canSend);
		}
		return false;
	}
	
	public function sent(){
		if($this->emailID){
			$db = SQL::DB();
			$stmt = false;
			
			try{
				$stmt = $db->prepare('UPDATE emails
SET timeSent=NOW()
WHERE emailID=:emailID');
				$stmt->bindParam(':emailID', $this->emailID, PDO::PARAM_INT);
				return $db->execute($stmt);
			}
			catch (PDOException $e){
				SQL::Error($e->getMessage(), $stmt);
			}
		}
		return false;
	}
	
	public function failed($message){
		if($this->emailID){
			$db = SQL::DB();
			$stmt = false;
			
			try{
				$stmt = $db->prepare('UPDATE emails
SET message=:message
WHERE emailID=:emailID');
				$stmt->bindParam(':message', $message, PDO::PARAM_STR);
				$stmt->bindParam(':emailID', $this->emailID, PDO::PARAM_INT);
				return $db->execute($stmt);
			}
			catch (PDOException $e){
				SQL::Error($e->getMessage(), $stmt);
			}
		}
		return false;
	}
}

class Emails{
	public static function Rate(){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT count(*) AS total FROM emails
WHERE timeQueued > NOW() - 60');
			if($db->execute($stmt)){
				return current($stmt->fetch());
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
	
	public static function CanSend(){
		$rate = Emails::Rate();
		if ($rate < Config::MAX_EMAILS_PER_MINUTE && $rate !== false){
			return true;
		}
		elseif($rate === false){
			return 'Database Error';
		}
		else{
			$tooManyMailError = new Error('Too many emails being sent', "Rate of: $rate");
			$tooManyMailError->forceEmail();
			$tooManyMailError->submit();
			
			return 'Exceeded mail per minute send rate';
		}
	}
	
	public static function Resend($emailID){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT * FROM emails
WHERE emailID = :emailID');
			$stmt->bindParam(':emailID', $emailID, PDO::PARAM_INT);
			if($db->execute($stmt)){
				$data = $stmt->fetch();
				return mail($data['to'], $data['subject'], $data['content'], $data['headers']);
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
	
	public static function Recent($count){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare('SELECT *
FROM emails
ORDER BY emailID DESC
LIMIT :count');
			$stmt->bindParam(':count', $count, PDO::PARAM_INT);
			
			if($db->execute($stmt)){
				return $stmt->fetchAll();
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
}