<?php
class Chat{
	public static function Messages($chatName){
		$db = SQL::DB();
		$stmt = false;
		
		try{
			$stmt = $db->prepare("SELECT lineID, userName, time, languageCode, message
FROM chatLines
JOIN accounts ON accounts.accountID = chatLines.accountID
JOIN chats ON chats.chatID = chatLines.chatID
WHERE chats.chatName = :chatName AND time > NOW() - INTERVAL 1 DAY");

      $stmt->bindParam(':chatName', $chatName, PDO::PARAM_STR);
			
			if($db->execute($stmt)){
				return $stmt->fetchAll();
			}
		}
		catch (PDOException $e){
			SQL::Error($e->getMessage(), $stmt);
		}
		return false;
	}
}
?>